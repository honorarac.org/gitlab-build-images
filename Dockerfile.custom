#
# The arguments in this file are configured in
# /scripts/custom-docker-build
#

ARG CUSTOM_IMAGE_NAME
ARG CUSTOM_IMAGE_VERSION
FROM ${CUSTOM_IMAGE_NAME}:${CUSTOM_IMAGE_VERSION}

ADD / /

RUN /scripts/install-essentials

ENV PATH $PATH:/usr/local/go/bin

# Git
ARG GIT_VERSION
ARG GIT_DOWNLOAD_URL=https://www.kernel.org/pub/software/scm/git/git-${GIT_VERSION}.tar.gz
ARG GIT_DOWNLOAD_SHA256

RUN if [ -n "$GIT_VERSION" ]; then /scripts/install-pcre2 && pcre2-config --version; fi
RUN if [ -n "$GIT_VERSION" ]; then /scripts/install-git && git --version; fi

# Chrome
ARG CHROME_VERSION
ARG CHROME_DRIVER_VERSION
RUN if [ -n "$CHROME_VERSION" ]; then /scripts/install-chrome $CHROME_VERSION $CHROME_DRIVER_VERSION && google-chrome --version; fi

# NodeJS and Yarn
ARG NODE_INSTALL_VERSION
ARG YARN_INSTALL_VERSION
RUN if [ -n "$NODE_INSTALL_VERSION" ] ; then /scripts/install-node $NODE_INSTALL_VERSION $YARN_INSTALL_VERSION && node --version && yarn --version; fi

# Golang
ARG INSTALL_GOLANG_VERSION
ARG GOLANG_DOWNLOAD_SHA256
RUN if [ -n "$INSTALL_GOLANG_VERSION" ] ; then /scripts/install-golang "${INSTALL_GOLANG_VERSION}" "${GOLANG_DOWNLOAD_SHA256}" && go version; fi

# Postgres
ARG POSTGRES_VERSION
RUN if [ -n "$POSTGRES_VERSION" ] ; then /scripts/install-postgresql $POSTGRES_VERSION; fi

# Ansible
ARG ANSIBLE_VERSION
RUN if [ -n "$ANSIBLE_VERSION" ] ; then /scripts/install-ansible $ANSIBLE_VERSION; fi

# Terraform
ARG TERRAFORM_VERSION
ARG TERRAFORM_DOWNLOAD_SHA256
RUN if [ -n "$TERRAFORM_VERSION" ] ; then /scripts/install-terraform $TERRAFORM_VERSION $TERRAFORM_DOWNLOAD_SHA256; fi

# GraphicsMagick
ARG GRAPHISMAGICK_VERSION
ARG GRAPHISMAGICK_DOWNLOAD_URL=https://sourceforge.net/projects/graphicsmagick/files/graphicsmagick/${GRAPHISMAGICK_VERSION}/GraphicsMagick-${GRAPHISMAGICK_VERSION}.tar.gz
ARG GRAPHISMAGICK_DOWNLOAD_SHA256
RUN if [ -n "$GRAPHISMAGICK_VERSION" ]; then /scripts/install-graphicsmagick && gm version; fi

# Docker
ARG DOCKER_VERSION
RUN if [ -n "$DOCKER_VERSION" ]; then /scripts/install-docker $DOCKER_VERSION; fi

RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
